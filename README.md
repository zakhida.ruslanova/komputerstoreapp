# The Komputer Store 
This is a dynamic webpage which allows the user to track his bank balance, take a loan, earn and transfer money and buy selected computer.

## Requirements
The application was build in "vanilla" JavaScript and Botstrap 5.

Visual StudioCode was used as text editor.

For testing and debugging Browser’s Developer Tools was used.


## Usage
To run the application you can use Live Server VS.