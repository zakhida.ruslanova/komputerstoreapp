import {
    balanceUserElement,
    getLoanUserElement,
    bankWorkElement,
    workWorkElement,
    buyElement,
    payElement,
    balanceLoanElement,
    repayLoanElement,
    priceElement,
    titelOfSelectedLaptopElement
} from "./constants.js";

export default function service() {
    let hasLoan = false
    let resBankBalanceUser = 0
    let resPayBalance = 0
    let resAmountOfLoan = 0
    let bankBalanceUser = 200
    let workMoney = 0
    let amountOfLoan = 0

    repayLoanElement.style.visibility = "hidden"

    const handleFormattedCurrency = (currency) => {
        return new Intl.NumberFormat('se-SE', { style: 'currency', currency: 'SEK' }).format(currency)
    }

    resBankBalanceUser = bankBalanceUser
    resPayBalance = workMoney
    resAmountOfLoan = amountOfLoan
    balanceUserElement.innerText = handleFormattedCurrency(resBankBalanceUser)
    payElement.innerText = handleFormattedCurrency(resPayBalance)
    balanceLoanElement.innerText = handleFormattedCurrency(resAmountOfLoan)

    const handlePayBalance = () => {
        resPayBalance = workMoney += 100
        payElement.innerHTML = handleFormattedCurrency(resPayBalance)
    }

    const handleGetALoan = () => {

        if (hasLoan === true) {
            alert("You cannot take a loan twice! Pay your previous loan and come back! ")
        } else {
            let amountOfLoanRequested = prompt("How much do you want to loan")
            amountOfLoan = amountOfLoanRequested
            if (amountOfLoan > (bankBalanceUser * 2) && amountOfLoan !== (bankBalanceUser * 2)) {
                alert("The current amount: " + amountOfLoanRequested + " is higher than you can get...")
            } else {
                resAmountOfLoan = amountOfLoan
                balanceLoanElement.innerText = handleFormattedCurrency(resAmountOfLoan)
                let totalSumOfLoanAndUserBalance = parseInt(amountOfLoan) + parseInt(bankBalanceUser)
                bankBalanceUser = totalSumOfLoanAndUserBalance
                balanceUserElement.innerText = handleFormattedCurrency(totalSumOfLoanAndUserBalance)
                hasLoan = true
                repayLoanElement.style.visibility = "visible"
            }
        }
    }

    const putRepayButtonIfUserHasALoan = (amountOfLoan) => {
        if (amountOfLoan === 0 || hasLoan === false) {
            repayLoanElement.style.visibility = "hidden"
        } else {
            repayLoanElement.style.visibility = "visible"
        }
    }

    const handleTransfer = () => {
        if (hasLoan !== true) {
            bankBalanceUser = parseInt(bankBalanceUser) + parseInt(workMoney);
            balanceUserElement.innerText = handleFormattedCurrency(bankBalanceUser)
            payElement.innerText = handleFormattedCurrency(0)
            workMoney = 0
        } else {
            let tenProcentOfAmountToLoan = workMoney / 10
            workMoney = workMoney - tenProcentOfAmountToLoan
            let amountRestOfLoan = amountOfLoan - tenProcentOfAmountToLoan
            amountOfLoan = amountRestOfLoan
            if (amountOfLoan <= 0) {
                amountOfLoan = Math.abs(amountOfLoan)
                bankBalanceUser += amountOfLoan + workMoney
                balanceUserElement.innerText = handleFormattedCurrency(bankBalanceUser)
                balanceLoanElement.innerText = handleFormattedCurrency(0)
                payElement.innerText = handleFormattedCurrency(0)
                workMoney = 0.0
                amountOfLoan = 0.0
                hasLoan = false
                repayLoanElement.style.visibility = "hidden"
            } else {
                let amountGoesToBankAccount = workMoney - tenProcentOfAmountToLoan
                let resBalanceUserElement = bankBalanceUser += amountGoesToBankAccount
                balanceUserElement.innerText = handleFormattedCurrency(resBalanceUserElement)
                balanceLoanElement.innerText = handleFormattedCurrency(amountOfLoan)
                payElement.innerText = 0.0
                workMoney = 0.0
                if (balanceLoanElement.innerText == 0.0) {
                    hasLoan = false
                }
            }
            putRepayButtonIfUserHasALoan(amountOfLoan)
        }
    }

    const handleTransferAllEarnedMoneyToLoan = () => {
        if (workMoney > 0) {
            if (resPayBalance > resAmountOfLoan) {
                let resBalanceWorkElement = resPayBalance -= resAmountOfLoan
                let modifiedLoan = resAmountOfLoan -= resAmountOfLoan
                let restOfErnedMoneay = parseInt(resBalanceWorkElement) + parseInt(bankBalanceUser)
                balanceLoanElement.innerText = handleFormattedCurrency(modifiedLoan)
                resPayBalance -= resPayBalance
                bankBalanceUser = restOfErnedMoneay
                balanceUserElement.innerText = handleFormattedCurrency(bankBalanceUser)
                payElement.innerText = handleFormattedCurrency(resPayBalance)
                workMoney = 0.0
                hasLoan = false

            } else {
                let resBalanceWorkElement = resAmountOfLoan -= resPayBalance
                balanceLoanElement.innerText = handleFormattedCurrency(resBalanceWorkElement)
                payElement.innerText = handleFormattedCurrency(resBalanceWorkElement)
                workMoney = 0.0
                hasLoan = false
            }
            putRepayButtonIfUserHasALoan(resAmountOfLoan)
        } else {
            alert("Your balance is empty!")
        }
    }

    const handleBuyItem = () => {

        let itemPrice = priceElement.innerText;

        if (bankBalanceUser > 0 && bankBalanceUser >= itemPrice) {
            let resBankBalanceUser = bankBalanceUser -= itemPrice
            balanceUserElement.innerText = handleFormattedCurrency(resBankBalanceUser)
            setTimeout(() => {
                alert("BOOOM! You bought it: " + titelOfSelectedLaptopElement.innerText + " the price: " + itemPrice);
            }, 1000);
        } else {
            alert("Oooops! You don't have enough money to buy: " + titelOfSelectedLaptopElement.innerText + " the price: " + itemPrice);
        }
    }

    buyElement.addEventListener("click", handleBuyItem);
    workWorkElement.addEventListener("click", handlePayBalance);
    bankWorkElement.addEventListener("click", handleTransfer);
    repayLoanElement.addEventListener("click", handleTransferAllEarnedMoneyToLoan);
    getLoanUserElement.addEventListener("click", handleGetALoan)
}