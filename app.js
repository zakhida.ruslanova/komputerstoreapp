import service from "./service.js";
import {
    laptopsElement,
    priceElement,
    featuresElement,
    descriptionElement,
    imageElement,
    titelOfSelectedLaptopElement,
} from "./constants.js";

service()
let computers = []
const imageUrl = "https://hickory-quilled-actress.glitch.me/";

fetch("https://hickory-quilled-actress.glitch.me/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputers(computers))

const addComputers = (computers) => {
    computers.forEach(x => addComputer(x))
    priceElement.innerHTML = computers[0].price
    let specs = computers[0].specs
    specs.forEach(spe => {
        featuresElement.appendChild(document.createElement('li')).innerHTML = spe
    });
    descriptionElement.innerHTML = computers[0].description
    titelOfSelectedLaptopElement.innerHTML = computers[0].title
    imageElement.src = imageUrl + computers[0].image
}

const addComputer = (computer) => {
    const computerElement = document.createElement("option")
    computerElement.value = computer.id
    computerElement.appendChild(document.createTextNode(computer.title))
    laptopsElement.appendChild(computerElement)
}

const handleComputerChange = e => {
    const selectedKompElement = computers[e.target.selectedIndex]
    let specs = selectedKompElement.specs
    featuresElement.innerHTML = "";
    specs.forEach(spe => {
        featuresElement.appendChild(document.createElement('li')).innerHTML = spe
    });
    priceElement.innerText = selectedKompElement.price
    descriptionElement.innerText = selectedKompElement.description
    titelOfSelectedLaptopElement.innerText = selectedKompElement.title
    imageElement.src = imageUrl + selectedKompElement.image
}

laptopsElement.addEventListener("change", handleComputerChange)